//
//  Vehicle.h
//  vehicleMagazine
//
//  Created by Andrey Chub on 4/26/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Vehicle : NSObject

@property (copy, nonatomic) NSString *objID;
@property (copy, nonatomic) NSString *mark;
@property (copy, nonatomic) NSString *model;
@property (copy, nonatomic) NSString *bodyType;
@property (copy, nonatomic) NSString *fuelType;
@property (copy, nonatomic) NSString *gearBox;
@property (copy, nonatomic) NSString *horsePower;
@property (copy, nonatomic) NSString *descriptionCar;
@property (copy, nonatomic) NSString *imageQBUrl;
@property (copy, nonatomic) NSString *createdAt;

@end
