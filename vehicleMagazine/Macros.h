//
//  Macros.h
//  Juelmin
//
//  Created by Andrey Chub on 3/10/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#ifndef Macros_h
#define Macros_h

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

//VIEW_CONTROLLERS

#define InstantiateViewControllerWithIdentifier(vcIdentifire) [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:vcIdentifire]


//SCREEN
#pragma mark - SCREEN

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)
#define IS_LANDSCAPE UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_3_5_INCH (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_4_INCH (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_4_7_INCH (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)

//COLORS
#pragma mark - COLORS

#define UIColorFromRGBWithAlpha(rgbValue, al) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:al]
#define UIColorFromRGB(rgbValue) UIColorFromRGBWithAlpha(rgbValue, 1.0)

#endif /* Macros_h */

