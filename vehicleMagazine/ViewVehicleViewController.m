//
//  ViewVehicleViewController.m
//  vehicleMagazine
//
//  Created by Andrey Chub on 4/26/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#import "ViewVehicleViewController.h"
#import "Macros.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ViewVehicleViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *vehicleImageView;
@property (weak, nonatomic) IBOutlet UILabel *markVehicleLabel;
@property (weak, nonatomic) IBOutlet UILabel *modelVehicleLabel;

@property (weak, nonatomic) IBOutlet UILabel *topLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *topRightLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomRightLabel;

@property (weak, nonatomic) IBOutlet UITextView *vehicleDescriptionTextView;

@end

@implementation ViewVehicleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.vehicleImageView.layer.borderWidth = 1;
    self.vehicleImageView.layer.borderColor = UIColorFromRGB(0xF8931D).CGColor;
    
    self.markVehicleLabel.text = self.vehicle.mark;
    self.modelVehicleLabel.text = self.vehicle.model;
    self.topLeftLabel.text = self.vehicle.bodyType;
    self.topRightLabel.text = self.vehicle.fuelType;
    self.bottomLeftLabel.text = self.vehicle.gearBox;
    self.bottomRightLabel.text = self.vehicle.horsePower;
    [self.vehicleImageView sd_setImageWithURL:[NSURL URLWithString:self.vehicle.imageQBUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
}

- (IBAction)closeBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
