//
//  QuickBloxManager.m
//  vehicleMagazine
//
//  Created by Andrey Chub on 4/26/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#import "QuickBloxManager.h"
#import "NSString+Rand.h"

@implementation QuickBloxManager

+ (instancetype)sharedInstance {
    
    static QuickBloxManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (void)postVehicle:(Vehicle *)vehicle success:(void(^)(NSString *vehicle))success failure:(void(^)(QBError *error))failure {
    
    QBCOCustomObject *object = [QBCOCustomObject customObject];
    object.className = @"Vehicle";
    
    [object.fields setObject:vehicle.mark != nil ? vehicle.mark : @"test"  forKeyedSubscript:@"Mark"];
    [object.fields setObject:vehicle.model != nil ? vehicle.model : @"test"  forKey:@"Model"];
    [object.fields setObject:vehicle.bodyType != nil ? vehicle.bodyType : @"test"  forKey:@"BodyTyp"];
    [object.fields setObject:vehicle.fuelType != nil ? vehicle.fuelType : @"test"  forKey:@"FuelType"];
    [object.fields setObject:vehicle.gearBox != nil ? vehicle.gearBox : @"test"  forKey:@"Gearbox"];
    [object.fields setObject:vehicle.horsePower != nil ? vehicle.horsePower : @"test"  forKey:@"Horsepower"];
    [object.fields setObject:vehicle.descriptionCar != nil ? vehicle.descriptionCar : @"test"  forKey:@"DescriptionCar"];
    [object.fields setObject:vehicle.imageQBUrl != nil ? vehicle.imageQBUrl : @"test"  forKey:@"imgeQuickBloxUrl"];
    
    [QBRequest createObject:object successBlock:^(QBResponse *response, QBCOCustomObject *object) {
        
        NSLog(@"%@", response);
        success(@"GOOD");
        
    } errorBlock:^(QBResponse *response) {
        
        NSLog(@"Response error: %@", [response.error description]);
    }];
    
}

- (void)getAllCarWithSuccess:(void(^)(NSMutableArray *mArray))success failure:(void(^)(QBError *error))failure {
    
    [QBRequest objectsWithClassName:@"Vehicle" successBlock:^(QBResponse *response, NSArray *objects) {
    
        NSLog(@"%@", objects);
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        for (QBCOCustomObject *qbObject in objects) {
            
            Vehicle *v = [[Vehicle alloc] init];
            v.mark = qbObject.fields[@"Mark"];
            v.model = qbObject.fields[@"Model"];
            v.bodyType = qbObject.fields[@"BodyTyp"];
            v.fuelType = qbObject.fields[@"FuelType"];
            v.gearBox = qbObject.fields[@"Gearbox"];
            v.horsePower = qbObject.fields[@"Horsepower"];
            v.descriptionCar = qbObject.fields[@"DescriptionCar"];
            v.imageQBUrl = qbObject.fields[@"imgeQuickBloxUrl"];
            v.objID = qbObject.ID;
            
            [array addObject:v];
        }
        success(array);
        
    }errorBlock:^(QBResponse *respons) {
        
        NSLog(@"%@", respons);
        failure(respons.error);
        
    }];
}

- (void)uploadImage:(UIImage *)image success:(void(^)(NSString *imageUrl))success failure:(void(^)(QBError *error))failure {
    
    
    NSData *imageData = UIImagePNGRepresentation(image);
    
    NSString *fileName = [NSString randomAlphanumericStringWithLength:10];
    
    [QBRequest TUploadFile:imageData fileName:fileName contentType:@"image/png" isPublic:YES successBlock:^(QBResponse *response, QBCBlob *blob) {
        // File uploaded, do something
        // if blob.isPublic == YES
        NSString *url = [blob publicUrl];
        success(url);
        
    } statusBlock:^(QBRequest *request, QBRequestStatus *status) {
        // handle progress
        
    } errorBlock:^(QBResponse *response) {
        NSLog(@"error: %@", response.error);
        failure(response.error);
    }];
}

- (NSURL *)databasePath
{
    return [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent:@"data.sqlite3"];
}

@end
