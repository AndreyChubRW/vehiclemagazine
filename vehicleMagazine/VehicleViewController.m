//
//  ViewController.m
//  vehicleMagazine
//
//  Created by Andrey Chub on 4/26/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#import "VehicleViewController.h"
#import "VehicleTableViewCell.h"
#import "Macros.h"
#import "NewVehicleViewController.h"
#import "ViewVehicleViewController.h"
#import "QuickBloxManager.h"

@interface VehicleViewController ()

@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchWidthConstraint;
@property (weak, nonatomic) IBOutlet UIButton *clearSearchTextBtn;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *tableData;
@property (strong, nonatomic) FMDatabase *db;

@end

@implementation VehicleViewController


#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    file:///Users/andreychub/Library/Developer/CoreSimulator/Devices/C3DCCD07-52A4-4757-9BA7-C37D0EC5EDDA/data/Containers/Data/Application/FE94B7C0-4146-43FF-A8D2-E7BBA77C67F9/Documents/data.sqlite3.0
    
    self.db = [FMDatabase databaseWithPath:[[self databasePath] absoluteString]];
    [self.db open];

    [self.db executeUpdate:@"CREATE TABLE IF NOT EXISTS Car (mark TEXT, model TEXT, bodyType TEXT, fuelType TEXT, gearBox TEXT, horsePower TEXT, descriptionCar TEXT)"];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [self loadListData];
}


#pragma mark - Action

- (IBAction)newVehicle:(id)sender {
    
    NewVehicleViewController *vc = InstantiateViewControllerWithIdentifier(@"NewVehicleViewController");
    [self.navigationController pushViewController:vc animated:YES];
}


- (IBAction)clearSearchTextBtnPressed:(id)sender {
}


#pragma mark - LoadData 

- (void)loadListData {
    
    [[QuickBloxManager sharedInstance] getAllCarWithSuccess:^(NSMutableArray *mArray) {
        
        self.tableData = [NSArray arrayWithArray:mArray];
        [self.tableView reloadData];
        
    }failure:^(QBError *error){
        
        NSString *SQL = [NSString stringWithFormat:@"SELECT * FROM Car"];
        FMResultSet *rs = [self.db executeQuery:SQL];
        NSMutableArray* arr = [NSMutableArray array];
        while ([rs next]) {
            Vehicle *c = [[Vehicle alloc]init];
            c.mark = [rs stringForColumnIndex:0];
            c.model = [rs stringForColumnIndex:1];
            c.bodyType = [rs stringForColumnIndex:2];
            c.fuelType = [rs stringForColumnIndex:3];
            c.gearBox = [rs stringForColumnIndex:4];
            c.horsePower = [rs stringForColumnIndex:5];
            c.descriptionCar = [rs stringForColumnIndex:6];
            
            [arr addObject:c]; 
        } 
        
        self.tableData = arr; 
        [self.tableView reloadData];
        
    }];
}


#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *VehicleTableViewCellIdentifire = @"VehicleTableViewCell";
    
    VehicleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:VehicleTableViewCellIdentifire];
    
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:VehicleTableViewCellIdentifire owner:self options:nil][0];
    }
    
    [cell setCellData:[self.tableData objectAtIndex:indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ViewVehicleViewController *vc = InstantiateViewControllerWithIdentifier(@"ViewVehicleViewController");
    vc.vehicle = [self.tableData objectAtIndex:indexPath.row];
    [self.navigationController presentViewController:vc animated:YES completion:nil];
}

@end
