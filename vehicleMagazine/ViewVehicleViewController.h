//
//  ViewVehicleViewController.h
//  vehicleMagazine
//
//  Created by Andrey Chub on 4/26/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Vehicle.h"

@interface ViewVehicleViewController : BaseViewController

@property (strong, nonatomic) Vehicle *vehicle;

@end
