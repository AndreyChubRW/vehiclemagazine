//
//  BaseViewController.m
//  vehicleMagazine
//
//  Created by Andrey Chub on 4/26/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
}

- (IBAction)navBackButtonPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSURL *)databasePath
{
    return [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent:@"data.sqlite3"];
}

@end
