//
//  QuickBloxManager.h
//  vehicleMagazine
//
//  Created by Andrey Chub on 4/26/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vehicle.h"
#import <UIKit/UIKit.h>
#import <FMDatabase.h>
#import <Quickblox/Quickblox.h>

@interface QuickBloxManager : NSObject

@property (strong, nonatomic) FMDatabase *db;

+ (instancetype)sharedInstance;
- (void)postVehicle:(Vehicle *)vehicle success:(void(^)(NSString *vehicle))success failure:(void(^)(QBError *error))failure;
- (void)getAllCarWithSuccess:(void(^)(NSMutableArray *mArray))success failure:(void(^)(QBError *error))failure;
- (void)uploadImage:(UIImage *)image success:(void(^)(NSString *imageUrl))success failure:(void(^)(QBError *error))failure;

@end
