//
//  NSString+Rand.m
//  vehicleMagazine
//
//  Created by Andrey Chub on 4/27/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#import "NSString+Rand.h"

@implementation NSString (Rand)

+ (NSString *)randomAlphanumericStringWithLength:(NSInteger)length
{
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity:length];
    
    for (int i = 0; i < length; i++) {
        [randomString appendFormat:@"%C", [letters characterAtIndex:arc4random() % [letters length]]];
    }
    
    return randomString;
}

@end
