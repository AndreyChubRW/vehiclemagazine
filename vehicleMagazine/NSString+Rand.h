//
//  NSString+Rand.h
//  vehicleMagazine
//
//  Created by Andrey Chub on 4/27/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Rand)

+ (NSString *)randomAlphanumericStringWithLength:(NSInteger)length;

@end
