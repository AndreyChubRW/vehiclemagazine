//
//  VehicleTableViewCell.m
//  vehicleMagazine
//
//  Created by Andrey Chub on 4/26/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#import "VehicleTableViewCell.h"
#import "Macros.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface VehicleTableViewCell ()


@property (weak, nonatomic) IBOutlet UIImageView *vehicleImageView;
@property (weak, nonatomic) IBOutlet UILabel *markVehicleLabel;
@property (weak, nonatomic) IBOutlet UILabel *modelVehicleLabel;

@property (weak, nonatomic) IBOutlet UILabel *topLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *topRightLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomRightLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end

@implementation VehicleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.vehicleImageView.layer.borderWidth = 1;
    self.vehicleImageView.layer.borderColor = UIColorFromRGB(0xF8931D).CGColor;
    self.spinner.hidden = NO;
    self.spinner.hidesWhenStopped = YES;
}

- (void)setCellData:(Vehicle *)data {
    
    [self.spinner startAnimating];
    
    self.markVehicleLabel.text = data.mark;
    self.modelVehicleLabel.text = data.model;
    self.topLeftLabel.text = data.bodyType;
    self.topRightLabel.text = data.fuelType;
    self.bottomLeftLabel.text = data.gearBox;
    self.bottomRightLabel.text = data.horsePower;
    [self.vehicleImageView sd_setImageWithURL:[NSURL URLWithString:data.imageQBUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        [self.spinner stopAnimating];
        
    }];
}

@end
