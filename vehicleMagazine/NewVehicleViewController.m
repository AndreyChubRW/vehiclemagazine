//
//  ViewVehicleViewController.m
//  vehicleMagazine
//
//  Created by Andrey Chub on 4/26/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#import "NewVehicleViewController.h"
#import "Macros.h"
#import "QuickBloxManager.h"
#import "Vehicle.h"
#import <SVProgressHUD.h>

@interface NewVehicleViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *vehicleImageView;
@property (weak, nonatomic) IBOutlet UIButton *addImageBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UITextField *vehicleMarkTextView;
@property (weak, nonatomic) IBOutlet UITextField *vehicleModelTextView;
@property (weak, nonatomic) IBOutlet UITextField *topLeftTextField;
@property (weak, nonatomic) IBOutlet UITextField *topRightTextField;
@property (weak, nonatomic) IBOutlet UITextField *bottomLeftTextField;
@property (weak, nonatomic) IBOutlet UITextField *bottomRightTextField;
@property (weak, nonatomic) IBOutlet UITextView *vehicleDescriptionTextView;

@property (strong, nonatomic) UIImage *pickedImage;

@end

@implementation NewVehicleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.vehicleImageView.layer.borderWidth = 1;
    self.vehicleImageView.layer.borderColor = UIColorFromRGB(0xF8931D).CGColor;
}

- (IBAction)saveBtnPressed:(id)sender {
    
    self.saveBtn.enabled = NO;
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    [[QuickBloxManager sharedInstance] uploadImage:self.pickedImage success:^(NSString *url){
        
        NSLog(@"%@", url);
        
        Vehicle *car = [[Vehicle alloc] init];
        car.mark = self.vehicleMarkTextView.text;
        car.model = self.vehicleModelTextView.text;
        car.bodyType = self.topLeftTextField.text;
        car.fuelType = self.topRightTextField.text;
        car.gearBox = self.bottomLeftTextField.text;
        car.horsePower = self.bottomRightTextField.text;
        car.imageQBUrl = url;
        car.descriptionCar = self.vehicleDescriptionTextView.text;
        
        [SVProgressHUD dismiss];
        
        [[QuickBloxManager sharedInstance] postVehicle:car success:^(NSString *car) {
            
            NSLog(@"%@", car);
            [self.navigationController popViewControllerAnimated:YES];
            
        } failure:^(QBError *error) {
            
            [SVProgressHUD dismiss];
            NSLog(@"%@", error);
        }];
        
        
    }failure:^(QBError *error){
        
        FMDatabase *db = [FMDatabase databaseWithPath:[[self databasePath] absoluteString]];
        [db open];
        
        NSString *Add = [NSString stringWithFormat:@"INSERT INTO Car (mark, model, bodyType, fuelType, gearBox, horsePower, descriptionCar) VALUES ('%@', '%@', '%@', '%@', '%@','%@', '%@')", self.vehicleMarkTextView.text, self.vehicleModelTextView.text, self.topLeftTextField.text, self.topRightTextField.text, self.bottomLeftTextField.text ,self.bottomRightTextField.text, self.vehicleDescriptionTextView.text];
        
        [db executeUpdate:Add];
        
        [SVProgressHUD dismiss];
    }];
    
}

- (IBAction)addNewImage:(id)sender {
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}


#pragma mark - UIImagePickerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    self.pickedImage = image;
    self.vehicleImageView.image = self.pickedImage;
    self.addImageBtn.hidden = YES;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
