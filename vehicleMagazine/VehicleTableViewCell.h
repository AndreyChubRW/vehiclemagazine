//
//  VehicleTableViewCell.h
//  vehicleMagazine
//
//  Created by Andrey Chub on 4/26/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Vehicle.h"

@interface VehicleTableViewCell : UITableViewCell

- (void)setCellData:(Vehicle *)data;

@end
