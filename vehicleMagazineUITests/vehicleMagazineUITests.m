//
//  vehicleMagazineUITests.m
//  vehicleMagazineUITests
//
//  Created by Andrey Chub on 4/26/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface vehicleMagazineUITests : XCTestCase

@end

@implementation vehicleMagazineUITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [app.navigationBars[@"VehicleView"].buttons[@"New Car"] tap];
    [app.navigationBars[@"NewVehicleView"].buttons[@"nav backbtn icon"] tap];
    
    
    
}

-(void)testExample3 {
    
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [app.navigationBars[@"VehicleView"].buttons[@"New Car"] tap];
    
    XCUIElementQuery *elementsQuery = app.scrollViews.otherElements;
    XCUIElement *textField = [[elementsQuery childrenMatchingType:XCUIElementTypeTextField] elementBoundByIndex:0];
    [textField tap];
    [textField typeText:@"Aston Martin"];
    
    XCUIElement *textField2 = [[elementsQuery childrenMatchingType:XCUIElementTypeTextField] elementBoundByIndex:1];
    [textField2 tap];
    [textField2 tap];
    [textField2 typeText:@"DB9"];
    
    XCUIElement *textField3 = [[elementsQuery childrenMatchingType:XCUIElementTypeTextField] elementBoundByIndex:2];
    [textField3 tap];
    [textField3 tap];
    [textField3 typeText:@"rtret"];
    
    XCUIElement *textField4 = [[elementsQuery childrenMatchingType:XCUIElementTypeTextField] elementBoundByIndex:3];
    [textField4 tap];
    [textField4 tap];
    [textField4 typeText:@"tret"];
    
    XCUIElement *textField5 = [[elementsQuery childrenMatchingType:XCUIElementTypeTextField] elementBoundByIndex:5];
    [textField5 tap];
    [textField5 tap];
    [textField5 typeText:@"tret"];
    
    XCUIElement *textField6 = [[elementsQuery childrenMatchingType:XCUIElementTypeTextField] elementBoundByIndex:4];
    [textField6 tap];
    [textField6 tap];
    [textField6 typeText:@"ert"];
    [elementsQuery.buttons[@"Add Image"] tap];
    [app.tables.buttons[@"Moments"] tap];
    [app.collectionViews.cells[@"Photo, Landscape, August 08, 2012, 2:55 PM"] tap];
    [elementsQuery.buttons[@"SAVE"] tap];
    
    
    NSAssert([app.tables.staticTexts containingType:XCUIElementTypeCell identifier:@"DB9"], @"fail");
    
    
}

@end
